# µmake

µmake is a small python script to easily and quickly compile small or medium-sized C/C++ projects. It is very light and designed to be modified to adapt to specific usage.

The advantages of this tool are :

* recursive search of all header/source files in their respective directories (like makefile wildcards)

* multiple binary output made easy (`app1` will be linked from `app1.cpp`, etc, without conflicts)

* hash system to avoid time check issues (same file content, remote files problems, etc)

* watch option to recompile whenever a source is changed


## Use

This repository is meant to be used as a submodule of a C/C++ project.

* Use `git submodule add` to include this tool anywhere in your project.

* Copy `makefile` and `make.py` templates in your project root folder.

* Update `UMAKE_PATH` in the copied `makefile` with the path to this submodule.

* In `make.py` update the `Config` template with the desired flags and options.

By default 3 targets can be used `make` (or `make all` as usual default of makefiles), `make debug` and `make clean`.

**Example :** from the root folder of a project :

```
git submodule add https://gitlab.com/corentin-pro/umake.git && cp umake/make* .
```

Then change the configuration in `make.py` and a simple `make` command will build your project!


## How it works

### Project layout

µmake expects all the header and source files to be place in a directories specified in the configuration (in `make.py`).
Object files and final binaries are outputed also in directories specified in the configuration but objects files in `debug` and `release` (default) are separated.

The separation of object/release objects files is meant to avoid rebuilding the whole project after debugging a single file.

To determine if a source file needs to be recompiled an hash is saved. The hashed are saved in a JSON file at the root of the object files directory (there are in fact 2 seperate files respectively in `debug` oand `release`).


### Caveats

* Knowing python is recommended to understand the tool for any usage above configuration change.

* Template configuration is meant for C++ files (cpp/hpp) for now.
