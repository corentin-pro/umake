MAKE_PATH="."
UMAKE_PATH="umake"

.PHONY: all debug clean

all:
	@PYTHONPATH=$(UMAKE_PATH) python3 $(MAKE_PATH)/make.py

debug:
	@PYTHONPATH=$(UMAKE_PATH) python3 $(MAKE_PATH)/make.py --type debug

clean:
	@PYTHONPATH=$(UMAKE_PATH) python3 $(MAKE_PATH)/make.py --clean
