#! python3

import os
from pathlib import Path

from umake import make


class Config:
    CC = 'g++'                             # Compiler to call
    APPS = ['app_name']                    # Output binaries (path to source without extension)
    IGNORE_APPS = []
    JOB_COUNT = int(os.cpu_count() * 0.8)  # Concurent jobs (multi-processing)
    WATCH = False                          # Watch source modification for auto-compiling

    BIN_DIR = Path('bin')          # Output directory (binaries)
    INCLUDE_DIR = Path('include')  # Include directory (header files)
    OBJECT_DIR = Path('obj')       # Temporary directory (object files)
    SOURCE_DIR = Path('src')       # Source directories

    COMMON_FLAGS = '-std=c++17'               # Flags used for comiling and linking
    COMMON_DEBUG_FLAGS = '-g'                 # Flags added in debug mode
    COMMON_RELEASE_FLAGS = '-O2 -flto'        # Flags added in release mode
    COMPILE_FLAGS = f'-Wall -I{INCLUDE_DIR}'  # Flags added for compiling (recommandation : `pkg-config --cflags`)
    LINK_FLAGS = ''                           # Flags added for linking (recommandation : `pkg-config --libs`)

    PRE_COMPILE_FUNCTION = None  # Function to run before compile (not call in --clean situation)

    CPP_SOURCES = [filepath for filepath in SOURCE_DIR.rglob('*.cpp') if not filepath.name.startswith('.')]


def main():
    make(Config)


if __name__ == '__main__':
    main()
